package com.rmplegendary.messenger.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static com.rmplegendary.messenger.util.Utils.readFromInputStream;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MessengerServiceTest {
    public static MessengerService messengerService;

    @BeforeAll
    static void setup() {
         messengerService = new MessengerService();
    }

    @Test
    public void checkTextInFile() throws IOException {
        String expectedText = "Hello Earth planet";
        Class<MessengerServiceTest> clazz = MessengerServiceTest.class;
        InputStream inputStream = clazz.getResourceAsStream("/myfile.txt");
        String data = readFromInputStream(inputStream);
        Assertions.assertThat(data).isEqualTo(expectedText);
    }

    @Test
    void activateFileMode() {
        boolean fileMode;
        fileMode = messengerService.activateFileMode(false);
        Assertions.assertThat(fileMode).isEqualTo(Boolean.FALSE);
    }

    @Test
    void latinTextValidator() {
        String notLatinText = "привет";
        assertThrows(RuntimeException.class, () -> messengerService.latinTextValidator(notLatinText));
    }

    @Test
    void emptyTextValidator() {
        assertThrows(RuntimeException.class, () -> messengerService.emptyTextValidator(""));
    }

    @Test
    void isStringOnlyAlphabet() {
        assertEquals(Boolean.TRUE, messengerService.isStringOnlyAlphabet("asdsdqweqweqc"));
    }
}