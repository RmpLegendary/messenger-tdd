package com.rmplegendary.messenger;

import com.rmplegendary.messenger.service.MessengerService;

import java.io.IOException;

public class MessengerApplication {
    public static void main(String[] args) {
        MessengerService messengerService = new MessengerService();
        boolean fileMode = messengerService.activateFileMode(false);
        if (Boolean.FALSE.equals(fileMode)) {
            System.out.println("Console Mode activated");
            messengerService.consolePersonalDataRequest();
        } else {
            System.out.println("File Mode activated");
            try {
                messengerService.copyToAnotherFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
