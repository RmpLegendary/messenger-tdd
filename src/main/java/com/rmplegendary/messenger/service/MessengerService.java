package com.rmplegendary.messenger.service;

import java.io.*;
import java.util.Objects;
import java.util.Scanner;

public class MessengerService {
    String filename;

    public boolean activateFileMode(boolean fileMode) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a file name: ");
        System.out.flush();
        while(scanner.hasNextLine()) {
            filename = scanner.nextLine();
        }
        if (filename != null && !"".equals(filename)) {
            fileMode = true;
        }
        return fileMode;
    }

    public void copyToAnotherFile() throws IOException {
        Objects.requireNonNull(filename);
        Scanner scn = new Scanner(System.in);
        System.out.print("Enter a target file name: ");
        System.out.flush();
        String targetFilename = scn.nextLine();
        File file = new File(filename + ".txt");
        if(!file.exists()){
            file.createNewFile();
            String str = "Hello Earth planet";
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(str);
            writer.close();
        }
        File targetFile = new File(targetFilename + ".txt");
        if(!targetFile.exists()){
            targetFile.createNewFile();
        }

        try (FileInputStream in = new FileInputStream(file); FileOutputStream out = new FileOutputStream(targetFile)) {
            int n;
            while ((n = in.read()) != -1) {
                out.write(n);
            }
            System.out.println("File Copied from " + filename + " to " + targetFilename);
        }
    }

    public void consolePersonalDataRequest() {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("Input your first name: ");
            String fname = input.nextLine();
            inputValidator(fname);
            System.out.print("Input your last name: ");
            String lname = input.nextLine();
            inputValidator(fname);
            System.out.println();
            System.out.println("Hello \n" + fname + " " + lname);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void latinTextValidator(String text) {
        if (Boolean.FALSE.equals(isStringOnlyAlphabet(text))) {
            throw new RuntimeException("Use only latin characters");
        }
    }

    public void emptyTextValidator(String text) {
        if (text.equals("")) {
            throw new RuntimeException("Cannot continue without your personal data!");
        }
    }

    public void inputValidator(String input) {
        emptyTextValidator(input);
        latinTextValidator(input);
    }

    public boolean isStringOnlyAlphabet(String str) {
        return !str.equals("") && str.matches("^[a-zA-Z]*$");
    }
}
